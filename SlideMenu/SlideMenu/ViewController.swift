//
//  ViewController.swift
//  SlideMenu
//
//  Created by iulian david on 7/11/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var leadingSpace: NSLayoutConstraint!
    @IBOutlet weak var menuView: UIView!
    
    var menuShown = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        let rightSwipe = UISwipeGestureRecognizer()
        rightSwipe.addTarget(self, action: #selector(showMenuSwipe(_:)))
        rightSwipe.numberOfTouchesRequired = 1
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(rightSwipe)
        
        
        let leftSwipe = UISwipeGestureRecognizer()
        leftSwipe.addTarget(self, action: #selector(hideMenuSwipe(_:)))
        leftSwipe.numberOfTouchesRequired = 1
        leftSwipe.direction = .left
        menuView.addGestureRecognizer(leftSwipe)
    }
    
    //    @IBAction func mySwiped(_ sender: UIGestureRecognizer) {
    //        print("My Swipe")
    //    }
    //
    //    func coolSwiped(_ swipe: UISwipeGestureRecognizer) {
    //        switch swipe.direction {
    //        case UISwipeGestureRecognizerDirection.left:
    //            myLabel.text = "Left Swipe"
    //        case UISwipeGestureRecognizerDirection.right:
    //            myLabel.text = "Right Swipe"
    //        case UISwipeGestureRecognizerDirection.down:
    //            myLabel.text = "Down Swipe"
    //        case UISwipeGestureRecognizerDirection.up:
    //            myLabel.text = "Up Swipe"
    //        default:
    //            break
    //        }
    //    }
    
    
    func showMenuSwipe(_ swipe: UISwipeGestureRecognizer) {
        guard !menuShown else {
            return
        }
        leadingSpace.constant = 0
        menuView.layer.shadowOpacity = 1
        menuView.layer.shadowOpacity = 10
        animateLayout()
    }
    
    func hideMenuSwipe(_ swipe: UISwipeGestureRecognizer) {
        guard menuShown else {
            return
        }
        leadingSpace.constant = -140
        
        animateLayout()
    }
    
    @IBAction func showMenu(_ sender: Any) {
        if menuShown {
            leadingSpace.constant = -140
            
        } else {
            leadingSpace.constant = 0
            menuView.layer.shadowOpacity = 1
            menuView.layer.shadowOpacity = 10
        }
        animateLayout()
    }
    
    fileprivate func animateLayout() {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
        menuShown = !menuShown
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        leadingSpace.constant = -140
        menuShown = false
    }
    
}

