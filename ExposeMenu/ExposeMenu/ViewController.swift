//
//  ViewController.swift
//  ExposeMenu
//
//  Created by iulian david on 7/11/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //contraints of the main view
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mainView: UIView!
    
    
    var menuShowing = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // adding shadow to create the aspect as on top
        mainView.layer.shadowOpacity = 1
        mainView.layer.shadowRadius = 10
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        if menuShowing {
        //            leadingConstraint.constant = 0
        //            trailingConstraint.constant = 0
        //        menuShowing = false
        //        }
    }
    @IBAction func showMenu(_ sender: Any) {
        if !menuShowing {
            //move the whole view over the set value
            self.leadingConstraint.constant = 100
            self.trailingConstraint.constant = -100
            
        } else {
            self.leadingConstraint.constant = 0
            self.trailingConstraint.constant = 0
            
        }
        UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: {
            self.view.layoutIfNeeded()
        })
        menuShowing = !menuShowing
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        leadingConstraint.constant = 0
        trailingConstraint.constant = 0
        menuShowing = false
    }
}

